#!/usr/bin/env bash

mkdir -p ~/QEMU-VMs/
ls -lah ~/QEMU-VMs/

read -e -r -p "create/run/delete (c/r/d): " op
read -e -r -p "vm name: " vmname

if [ "$op" == "c" ]; then
    read -e -r -p "storage: " vmstorage
    read -e -r -p "installation medium (full path): " medium
    read -e -r -p "ram: " ram
    read -e -r -p "cpu cores: " cores

    qemu-img create -f qcow2 ~/QEMU-VMs/"$vmname" "$vmstorage"
    qemu-system-x86_64 -enable-kvm -boot menu=on -cpu host -vga virtio -m "$ram" -smp "$cores" ~/QEMU-VMs/"$vmname" -cdrom "$medium"

fi

if [ "$op" == "r" ]; then
    read -e -r -p "ram: " ram
    read -e -r -p "cpu cores: " cores

    qemu-system-x86_64 -enable-kvm -boot menu=on -cpu host -vga virtio -m "$ram" -smp "$cores" ~/QEMU-VMs/"$vmname"
fi

if [ "$op" == "d" ]; then
    rm -rf ~/QEMU-VMs/"$vmname"
fi
